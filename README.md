# StageVerslagen

## 2018-10-01 (Dag 1)

We hebben vandaag een computer helemaal in elkaar gezet en op die zelfde pc een windows server geïnstalleerd. Hiervoor hebben we Rufus gebruikt om een opstartstick te maken.
We hebben vervolens een uitgebreide uitleg gekregen over servers die in de herstel afdeling waren beland. Ook hebben we geöbserveerd hoe een medewerker een scherm van een Ipad verving door een gloednieuw scherm.
Tenslotte hebben we een kijk genomen in de winkel om het assortiment van de winkel te bekijken.

## 2018-10-02 (Dag 2)

We zijn de dag begonnen met een active directory te maken op onze windows server. Vervolgens hebben we ook meteen een DHCP server geïnstalleerd. Hierna hebben we een analyse gemaakt van een binnegebrachte printer
en een binne gebrachte laptop (beide waren kapot). We hebben een lange tijd gewerkt aan onze GIP-taken. Tenslotte hebben we met onze eigen server gewerkt en hebben een klein netwerk gemaakt met een user (zie screenshots) die we bepaalde policies hebben gegeven.

## 2018-10-03 (Dag 3)

Vandaag hebben we onze case study afgewerkt met onze monitor. We hebben een webserver laten draaien op één van de laptops. Hiervoor hebben we het systeem IIS(internet information system) gebruikt. Heb daarna ook heel de case study in google docs uitgetypt. In de namiddag hebben we de foto's voor frans getrokken en hebben we een Imac langs de binnekant gezien.
De Imac was zelfs zo oud dat er een vlieg in zat en lag helemaal onder het stof.

## 20110-04 (Dag 4 en 5)

De laatste 2 dagen was het rustig in de winkel we hebben onze taak van frans afgemaakt. Verder zijn er zeven kapotte laptops binnengebracht waarvan na een analyse 6 van de 7 niet eens kapot waren maar bijvoorbeeld de batterijen van plat waren.
Tenslotte heb ik nog wat aan mijn case study gewerkt om deze te verbeteren. We hebben onze taken afgekregen en verbeterd.